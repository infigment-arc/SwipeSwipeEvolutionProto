﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollDown : MonoBehaviour
{
    public static readonly float SCROLL_TIME_MS = 3400;                      //< Approximate time for the swipe to reach the end of the screen

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += (Vector3.down * 3) * Time.deltaTime;
    }
}
