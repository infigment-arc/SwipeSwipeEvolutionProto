﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class BeatsheetReader
{
    private static readonly char[] PART_SEPARATOR = new[] { ':' };
    private static readonly string[] LINE_SEPARATOR = new[] { "\n" };

    public static Queue<KeyValuePair<int, SwipeObject.eDirection>> ReadBeatsheet(TextAsset beatsheet)
    {
        Queue<KeyValuePair<int, SwipeObject.eDirection>> parsedSheet = new Queue<KeyValuePair<int, SwipeObject.eDirection>>();
        string[] lines = beatsheet.text.Split(LINE_SEPARATOR, StringSplitOptions.RemoveEmptyEntries);
        foreach(string line in lines)
        {
            string[] parts = line.Trim().Split(PART_SEPARATOR);
            int millisecond = int.Parse(parts[0]);
            SwipeObject.eDirection direction = ParseDirection(parts[1]);
            parsedSheet.Enqueue(new KeyValuePair<int, SwipeObject.eDirection>(millisecond, direction));
        }
        
        return parsedSheet;
    }

    private static SwipeObject.eDirection ParseDirection(string directionCharacter)
    {
        switch (directionCharacter)
        {
            case "L":
                return SwipeObject.eDirection.LEFT;
            case "R":
                return SwipeObject.eDirection.RIGHT;
            case "U":
                return SwipeObject.eDirection.UP;
            case "D":
                return SwipeObject.eDirection.DOWN;
            default:
                throw new ArgumentException("Oh no! Bad character while trying to parse beat sheet!");
        }
    }
}
