﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPreviewController : MonoBehaviour
{
    private AudioSource audioSource;
    private AudioTrack currentTrack;
    private readonly float PREVIEW_LENGTH = 10.0f;
    private readonly float FADE_LENGTH = 2.0f;
    private readonly float SILENCE_LENGTH = 4.0f;

    public void Awake()
    {
        audioSource = GameObject.Find("SoundManager").GetComponent<AudioSource>();
        audioSource.volume = 0;
    }

    public void Update()
    {
        if(audioSource.isPlaying)
        {
            float timePlayed = audioSource.time;
            if(timePlayed < FADE_LENGTH)
            {
                audioSource.volume = Mathf.Lerp(0, 1, timePlayed / FADE_LENGTH);
            }

            float timePastPreview = timePlayed - PREVIEW_LENGTH;
            if(timePastPreview >= 0)
            {
                audioSource.volume = Mathf.Lerp(1, 0, timePastPreview/FADE_LENGTH);
            }

            float resetTime = PREVIEW_LENGTH + SILENCE_LENGTH;
            if(timePlayed >= resetTime)
            {
                audioSource.Stop();
                audioSource.Play();
                audioSource.volume = 0;
            }
        }
    }

    public void PreviewTrack(AudioTrack audioTrack)
    {
        currentTrack = audioTrack;
        audioSource.clip = audioTrack.AudioClip;
        audioSource.Play();
    }
}
