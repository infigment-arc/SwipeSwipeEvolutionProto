﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUDCollection : MonoBehaviour
{
    private List<Transform> HUDs = new List<Transform>();

    public void JoinPlayerHUD(Transform playerHUD)
    {
        int index = HUDs.Count;
        int hudHeight = 60;
        HUDs.Add(playerHUD);

        int baseX = -50;
        int baseY = -75;
        playerHUD.SetParent(transform, false);
        RectTransform rectTransform = playerHUD.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(baseX, baseY - (index * hudHeight));
    }
}
