﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTimeCounter : MonoBehaviour
{
    public AudioSource AudioSource;

    public void OnGUI()
    {
        int timeInMillis = (int)(AudioSource.time * 1000);
        GUI.Label(new Rect(0, 0, 100, 100), timeInMillis.ToString());
    }
}
