﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrackInfoSpawner : MonoBehaviour
{
    public GameObject trackInfoPrefab;
    private TrackInfo trackInfo;

    void Start()
    {
        if(GameObject.Find("TrackInfo(Clone)") == null && NetworkServer.active)
        {
            GameObject createdTrackInfo = Instantiate(trackInfoPrefab);
            NetworkServer.Spawn(createdTrackInfo);
        }
        trackInfo = GameObject.Find("TrackInfo(Clone)").GetComponent<TrackInfo>();
    }

    public void ButtonClicked(int index)
    {
        trackInfo.SetTrack(index);
    }

    public void ButtonSpeedSelectionClicked(float speed)
    {
        trackInfo.SetTrackSpeed(speed);
    }

    public void ButtonSwipeSelectionClicked(int mode)
    {
        trackInfo.SetSwipeMode(mode);
    }
}
