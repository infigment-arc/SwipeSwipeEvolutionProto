﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoomPlayerInterface : MonoBehaviour
{
    public GameObject serverButtonPrefab;
    public GameObject playerPanelPrefab;

    NetworkRoomPlayer networkRoomPlayer;
    GameObject playerPanel;
    GameObject readyButton;

    private bool readyState = false;

    public void Start()
    {
        networkRoomPlayer = GetComponent<NetworkRoomPlayer>();
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitializeRoomScene();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
        InitializeRoomScene();
    }

    private void InitializeRoomScene()
    {
        NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
        if (NetworkManager.IsSceneActive(room.RoomScene))
        {
            if (NetworkClient.active && networkRoomPlayer.isLocalPlayer)
            {
                readyButton = CreateReadyButton();
            }
            CreatePlayerPanel();
        }
    }

    public void OnDestroy()
    {
        Destroy(playerPanel);
    }

    private void CreatePlayerPanel()
    {
        GameObject canvas = GameObject.Find("Canvas");
        playerPanel = Instantiate(playerPanelPrefab, canvas.transform);
        RectTransform rectTransform = playerPanel.GetComponent<RectTransform>();

        float left = 110 - (Screen.width / 2) ;
        rectTransform.anchoredPosition = new Vector2(left + (networkRoomPlayer.index * 150), 100);

        playerPanel.transform.Find("PlayerName").GetComponent<Text>().text = $"Player [{networkRoomPlayer.index + 1}]";

        if (((networkRoomPlayer.isServer && networkRoomPlayer.index > 0) || networkRoomPlayer.isServerOnly))
        {
            //Find and set up button
            playerPanel.transform.Find("KickButton").GetComponent<Button>().onClick.AddListener(GetComponent<NetworkIdentity>().connectionToClient.Disconnect);
        }
        else
        {
            //Hide Button
            playerPanel.transform.Find("KickButton").gameObject.SetActive(false);
        }
    }

    private GameObject CreateReadyButton()
    {
        GameObject canvas = GameObject.Find("Canvas");
        GameObject serverButton = Instantiate(serverButtonPrefab, canvas.transform);
        serverButton.GetComponentInChildren<Text>().text = networkRoomPlayer.readyToBegin ? "Cancel" : "Ready";
        RectTransform rectTransform = serverButton.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(0, 0);
        serverButton.GetComponent<Button>().onClick.AddListener(OnReadyActionToggled);

        return serverButton;
    }

    private void OnReadyActionToggled()
    {
        networkRoomPlayer.CmdChangeReadyState(!networkRoomPlayer.readyToBegin);
    }

    private void ReadyStateChanged()
    {
        if(playerPanel)
        {
            playerPanel.transform.Find("ReadyState").GetComponent<Text>().text = readyState ? "Ready" : "Not Ready";
        }

        if(readyButton && networkRoomPlayer.isLocalPlayer)
        {
            readyButton.GetComponentInChildren<Text>().text = readyState ? "Cancel" : "Ready";
        }
    }

    public void Update()
    {
        if(networkRoomPlayer.readyToBegin != readyState)
        {
            readyState = networkRoomPlayer.readyToBegin;
            ReadyStateChanged();
        }
    }
}
