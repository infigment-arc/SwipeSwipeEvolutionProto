﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class LogHelper
{
    public static void LogDebug(string log, bool writeFile = false)
    {
        Debug.Log(log);

        if(!writeFile)
        {
            return;
        }

        using (StreamWriter streamWriter = new StreamWriter("swipe_log.txt", true))
        {
            streamWriter.WriteLine(log);
        }
    }
}
