﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamEnd : MonoBehaviour
{
    public ScoreSource scoreSource;
    public SongEventSource songEventSource;
    public GameObject scoreUIGroup;
    public Text score;
    public Text grade;
    public SwipeSpawner swipeSpawner;

    private NetworkRoomManager manager;
    private float maxSwipeScore;

    void Start()
    {
        songEventSource.OnSongEnded += SongEnded;
        manager = GameObject.Find("NetworkManager").GetComponent<NetworkRoomManager>();
        maxSwipeScore = CaclulateMaxSwipeScore();
    }

    private float CaclulateMaxSwipeScore()
    {
        float incrementsPerSecond = 1 / SwipeObject.SCORE_INCREMENT_PERIOD_SECONDS;
        float scrollTimeSecs = ScrollDown.SCROLL_TIME_MS / 1000;
        return SwipeObject.SCORE_INCREMENT * (incrementsPerSecond * scrollTimeSecs);
    }

    public void OnDestroy()
    {
        songEventSource.OnSongEnded -= SongEnded;
    }

    public void OnSelectEnd()
    {
        manager.ServerChangeScene(manager.RoomScene);
    }

    private void SongEnded(object sender, SongEndEventArgs eventArgs)
    {
        string calculatedGrade = CalculateGrade(swipeSpawner.totalBeatCount, scoreSource.score);

        score.text = scoreSource.score.ToString();
        grade.text = calculatedGrade;

        scoreUIGroup.SetActive(true);

        string songTitle = songEventSource.currentSong.TrackName;
        SaveScore(songTitle, scoreSource.score, calculatedGrade);
    }

    private void SaveScore(string songTitle, int score, string calculatedGrade)
    {
        if (SaveSystem.LoadSongScore(songTitle).Item1 < scoreSource.score)
        {
            SaveSystem.SaveSongScore(songTitle, scoreSource.score, calculatedGrade);
        }
    }

    private string CalculateGrade(int? totalBeatCount, int score)
    {
        if(!totalBeatCount.HasValue)
        {
            return "S?";
        }

        string calculatedGrade = "F";
        float maxScore = totalBeatCount.Value * maxSwipeScore;

        if (score >= maxScore*0.5)
        {
            calculatedGrade = "E";
        }
        if (score >= maxScore*0.6)
        {
            calculatedGrade = "D";
        }
        if (score >= maxScore*0.7)
        {
            calculatedGrade = "C";
        }
        if (score >= maxScore*0.8)
        {
            calculatedGrade = "B";
        }
        if (score >= maxScore*0.9)
        {
            calculatedGrade = "A";
        }
        if (score >= maxScore*0.98)
        {
            calculatedGrade = "S";
        }

        Debug.Log($"S Rank {maxScore * 0.98}");

        return calculatedGrade;
    }
}
