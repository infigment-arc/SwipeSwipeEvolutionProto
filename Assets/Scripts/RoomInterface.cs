﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RoomInterface : MonoBehaviour
{
    public GameObject serverButtonPrefab;
    GameObject createdStopButton;

    NetworkManager manager;

    bool isServer = false;
    bool isClient = false;

    private GameObject CreateStopButton(UnityAction stopAction, string serverType)
    {
        GameObject serverButton = Instantiate(serverButtonPrefab, transform);
        serverButton.GetComponentInChildren<Text>().text = $"Stop {serverType}";
        RectTransform rectTransform = serverButton.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(0,240);
        serverButton.GetComponent<Button>().onClick.AddListener(stopAction);

        return serverButton;
    }

    // Update is called once per frame
    void Update()
    {
        bool valueChanged = false;
        if(NetworkServer.active != isServer)
        {
            isServer = NetworkServer.active;
            valueChanged = true;
        }

        if (NetworkClient.isConnected != isClient)
        {
            isClient = NetworkClient.isConnected;
            valueChanged = true;
        }

        if(valueChanged)
        {
            OnServerTypeChanged();
        }
    }

    private void OnServerTypeChanged()
    {
        if(createdStopButton != null)
        {
            Destroy(createdStopButton);
        }

        GameObject networkManger = GameObject.Find("NetworkManager");
        manager = networkManger.GetComponent<NetworkManager>();

        if (isServer && isClient)
        {
            createdStopButton = CreateStopButton(() => manager.StopHost(), "Host");
        }
        // stop client if client-only
        else if (isClient)
        {
            createdStopButton = CreateStopButton(() => manager.StopClient(), "Client");
        }
        // stop server if server-only
        else if (isServer)
        {
            createdStopButton = CreateStopButton(() => manager.StopServer(), "Server");
        }
    }
}
