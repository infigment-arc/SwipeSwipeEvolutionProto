﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioTrack
{
    public AudioClip AudioClip { get; set; }
    public string TrackName { get; set; }
    public float TrackLength { get; set; }
    public float TrackOutPoint { get; set; }
    public float TrackSpeed { get; set; }
    public TextAsset Beatsheet { get; set; } = null;
}
