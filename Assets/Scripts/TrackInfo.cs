using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class TrackInfo : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnTrackUpdated))]
    public int trackIndex = -1;

    [SyncVar(hook = nameof(OnSwipeModeUpdated))]
    public int swipeMode = 0;

    [SyncVar(hook = nameof(OnTrackSpeedUpdated))]
    public float swipeSpeed = 1.0f;

    private AudioTrack audioTrack;
    private AudioTrackData trackData;

    public void Awake()
    {
        LogHelper.LogDebug("Track Info has been created", true);
        trackData = GetComponent<AudioTrackData>();
        trackIndex = -1;
        DontDestroyOnLoad(gameObject);
    }

    public override void OnStartServer()
    {
        OnTrackUpdated(trackIndex, 0);
        OnTrackSpeedUpdated(swipeSpeed, 1.0f);
        OnSwipeModeUpdated(0, 1);
    }

    public AudioTrack GetTrack()
    {
        return audioTrack;
    }

    private void SetTrackNameText(string trackName)
    {
        GameObject.Find("TrackName").GetComponent<Text>().text = trackName;
        (float highscore, string grade) = SaveSystem.LoadSongScore(trackName);
        GameObject.Find("Highscore").GetComponent<Text>().text = highscore.ToString();
        GameObject.Find("Grade").GetComponent<Text>().text = grade;
    }

    private void SetSpeedText(float speed)
    {
        GameObject.Find("SelectedSpeed").GetComponent<Text>().text = speed.ToString();
    }

    private void SetSwipeModeText(int mode)
    {
        string type = mode == 0 ? "Single" : "Multi";
        GameObject.Find("SelectedSwipeMode").GetComponent<Text>().text = type;
    }

    public void SetTrack(int index)
    {
        if(!NetworkServer.active)
        {
            LogHelper.LogDebug("Server is not active. Not setting track", true);
            return;
        }

        LogHelper.LogDebug($"Track index updated to {index}", true);
        trackIndex = index;
    }

    public void SetTrackSpeed(float speed)
    {
        swipeSpeed = speed;
    }

    public void SetSwipeMode(int mode)
    {
        swipeMode = mode;
    }

    private void OnTrackUpdated(int oldIndex, int newIndex)
    {
        LogHelper.LogDebug($"Track updated from {oldIndex} to {newIndex}", true);
        trackIndex = newIndex;
        audioTrack = trackData.tracks[trackIndex];
        audioTrack.TrackSpeed = swipeSpeed;             //< This is so we can configure the speed. Really this value would be static.

        SetTrackNameText(audioTrack.TrackName);
        SetSpeedText(audioTrack.TrackSpeed);
        SetSwipeModeText(swipeMode);
        PreviewTrack(audioTrack);
    }

    private void OnSwipeModeUpdated(int oldMode, int newMode)
    {
        swipeMode = newMode;
        SetSwipeModeText(swipeMode);
    }

    private void OnTrackSpeedUpdated(float oldSpeed, float newSpeed)
    {
        swipeSpeed = newSpeed;
        audioTrack.TrackSpeed = newSpeed;
        SetSpeedText(swipeSpeed);
    }

    private void PreviewTrack(AudioTrack audioTrack)
    {
        GameObject.Find("SoundManager").GetComponent<TrackPreviewController>().PreviewTrack(audioTrack);
    }

}
