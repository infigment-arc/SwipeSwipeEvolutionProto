using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeSpawner : MonoBehaviour
{
    public GameObject[] m_swipePrefabs;                                     //< Potential swipe actions.
    public SongEventSource songEventSource;                                 //< Information about the current track.
    private bool ended = false;                                             //< Stores whether the track has finished playing.
    private bool hasBeatsheet = false;                                      //< Whether the track info has a beatsheet to read the swipes from
    private Queue<KeyValuePair<int, SwipeObject.eDirection>> beatsheet;     //< Parsed beat sheet if one was read from the track info on start

    public int? totalBeatCount = null;

    void Start()
    {
        songEventSource.OnSongEnded += SongEnded;

        if (!songEventSource.currentSong.Beatsheet)
        {
            SetBeatCount(CalculateAutoSpawnBeatCount());
            StartCoroutine(SpawnNextMove());
            return;
        }

        hasBeatsheet = true;
        beatsheet = BeatsheetReader.ReadBeatsheet(songEventSource.currentSong.Beatsheet);
        SetBeatCount(beatsheet.Count);
    }

    private int CalculateAutoSpawnBeatCount()
    {
        return (int)(songEventSource.currentSong.TrackOutPoint / songEventSource.currentSong.TrackSpeed);
    }

    private void SetBeatCount(int count)
    {
        totalBeatCount = count;
    }

    public void OnDestroy()
    {
        songEventSource.OnSongEnded -= SongEnded;
    }

    public void Update()
    {
        if (!hasBeatsheet)
        {
            return;
        }

        //Find time in milliseconds. This is a naive implementation as a first pass
        //This could go wrong pretty easily. But it'll do for now
        float timeInMilliseconds = songEventSource.audioSource.time * 1000;
        while (ShouldSpawnNextSwipe(timeInMilliseconds))
        {
            KeyValuePair<int, SwipeObject.eDirection> directionPair = beatsheet.Dequeue();
            int index = DirectionToIndex(directionPair.Value);
            Instantiate(m_swipePrefabs[index]);
        }
    }

    private bool ShouldSpawnNextSwipe(float timeInMilliseconds)
    {
        if(beatsheet.Count <= 0)
        {
            return false;
        }

        // Include the time it takes to scroll so that the time that the player should hit
        // it for the optimal points arrives when the beat sheet specifies
        float beatTime = beatsheet.Peek().Key - ScrollDown.SCROLL_TIME_MS;
        return timeInMilliseconds >= beatTime;
    }

    IEnumerator SpawnNextMove()
    {
        //! Make sure we always spawn the moves in the same order.
        UnityEngine.Random.InitState(1000);
        while(!ended)
        {

            songEventSource.SwipeMode();

            yield return new WaitForSeconds(songEventSource.currentSong.TrackSpeed);

            if(m_swipePrefabs.Length > 0)
            {
                //! Select the prefab to instantiate.
                int index = UnityEngine.Random.Range(0, m_swipePrefabs.Length);
                bool doubleSpawn = UnityEngine.Random.Range(0, 100) >= 90;
                if (songEventSource.SwipeMode() == 1 && doubleSpawn)
                {
                    int secondIndex = index;
                    while (secondIndex == index)
                    {
                        secondIndex = UnityEngine.Random.Range(0, m_swipePrefabs.Length);
                    }

                    GameObject.Instantiate(m_swipePrefabs[secondIndex]);
                }

                //! Create the swipe object prefab
                GameObject.Instantiate(m_swipePrefabs[index]);
            }
        }
    }

    private void SongEnded(object sender, SongEndEventArgs eventArgs)
    {
        ended = true;
    }

    private int DirectionToIndex(SwipeObject.eDirection direction)
    {
        switch (direction)
        {
            case SwipeObject.eDirection.LEFT:
                return 3;
            case SwipeObject.eDirection.RIGHT:
                return 4;
            case SwipeObject.eDirection.UP:
                return 7;
            case SwipeObject.eDirection.DOWN:
                return 0;
            default:
                throw new ArgumentException("Uh oh! We don't have this direction in stock!");
        }
    }
}
