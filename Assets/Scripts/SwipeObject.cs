﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeObject : MonoBehaviour
{
    public static readonly float SCORE_INCREMENT = 10.0f;
    public static readonly float SCORE_INCREMENT_PERIOD_SECONDS = 0.1f;

    public enum eDirection
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        DOWN_LEFT,
        DOWN_RIGHT,
        TOP_LEFT,
        TOP_RIGHT
    };
    public eDirection m_swipeDirection;

    public float m_score;
    private SpriteRenderer m_renderer;

    public void Awake()
    {
        m_renderer = gameObject.GetComponent<SpriteRenderer>();
        StartCoroutine(UpdateScore());
    }

    public bool IsVisible()
    {
        return m_renderer.isVisible;
    }

    IEnumerator UpdateScore()
    {
        while (true)
        {
            m_score += SCORE_INCREMENT;
            yield return new WaitForSeconds(SCORE_INCREMENT_PERIOD_SECONDS); //! 10 times a second?
        }
    }
}
