﻿using Mirror;
using Mirror.Discovery;
using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class StartGameInterface : MonoBehaviour
{
    public GameObject serverButtonPrefab;
    public Text ipInput;

    NetworkManager manager;
    NetworkDiscovery networkDiscovery;
    readonly Dictionary<long, ServerResponse> discoveredServers = new Dictionary<long, ServerResponse>();
    List<GameObject> serverButtons = new List<GameObject>();


    void Awake()
    {
        GameObject networkManger = GameObject.Find("NetworkManager");
        manager = networkManger.GetComponent<NetworkManager>();
        networkDiscovery = networkManger.GetComponent<NetworkDiscovery>();
        networkDiscovery.OnServerFound.AddListener(OnServerDiscovered);
    }

    public void StartOnlineHost()
    {
        manager.StartHost();
    }

    public void StartOnlineClient()
    {
        manager.StartClient();
    }

    public void StartOnlineServer()
    {
        manager.StartServer();
    }

    public void OnHostIpUpdated(string ip)
    {
        manager.networkAddress = ipInput.text;
    }

    public void FindLocalServers()
    {
        ClearServerButtons();
        networkDiscovery.StartDiscovery();
    }

    public void StartLocalHost()
    {
        ClearServerButtons();
        NetworkManager.singleton.StartHost();
        networkDiscovery.AdvertiseServer();
    }

    public void OnServerDiscovered(ServerResponse info)
    {
        // Note that you can check the versioning to decide if you can connect to the server or not using this method
        if(!discoveredServers.ContainsKey(info.serverId))
        {
            discoveredServers[info.serverId] = info;
            AddServerButton(discoveredServers.Keys.Count-1, info.uri, info.EndPoint.Address);
        }
    }

    private void AddServerButton(int count, Uri uri, IPAddress address)
    {
        GameObject serverButton = Instantiate(serverButtonPrefab, transform);
        serverButton.GetComponentInChildren<Text>().text = address.ToString();
        RectTransform rectTransform = serverButton.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(0, 120 - (count * 40));
        serverButton.GetComponent<Button>().onClick.AddListener(() => OnServerButtonClicked(uri));
    }

    private void ClearServerButtons()
    {
        discoveredServers.Clear();
        for(int i=0; i<serverButtons.Count; ++i)
        {
            Destroy(serverButtons[i]);
        }
        serverButtons.Clear();
    }

    private void OnServerButtonClicked(Uri uri)
    {
        NetworkManager.singleton.StartClient(uri);
    }
}
