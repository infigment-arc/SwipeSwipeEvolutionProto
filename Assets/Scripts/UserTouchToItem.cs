﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class UserTouchToItem : MonoBehaviour
{
	private Vector2 touch_start_position;
	private Vector2 touch_end_position;
	private float angle = 0;

    public Action<float> OnSwipeObjectDestroyed;
	public GameObject trail_parent;

	private TrailRenderer trail_renderer;


    private void Awake()
    {
		trail_renderer = trail_parent.GetComponent<TrailRenderer>();
    }

    private void OnMouseDown()
	{
		touch_start_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		ResetTrail();
	}

    private void OnMouseDrag()
    {
		//! Update the trail renderer position
		if(trail_parent != null)
        {
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePosition.z = trail_parent.transform.position.z;
			trail_parent.transform.position = mousePosition;
		}
    }

    private void OnMouseUp()
	{
		touch_end_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		CalculateSwipe();
	}

	private void ResetTrail()
    {
		//trail_renderer.Clear();

		if (trail_parent != null)
		{
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePosition.z = trail_parent.transform.position.z;
			trail_parent.transform.position = mousePosition;
		}
		
		trail_renderer.Clear();
	}

	private void CalculateSwipe()
	{		
		SwipeObject nextSwipeObject = GetNextSwipe();

        if(!nextSwipeObject)
        {
            return;
        }

		angle = Mathf.Atan2(touch_end_position.y - touch_start_position.y, touch_end_position.x - touch_start_position.x) / Mathf.PI;


		//Each range is 90 degree
		if (angle > 0.375f && angle < 0.625f)
		{
			//Up Swipe
			Debug.Log("Up swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.UP)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
		else if (angle < -0.375f && angle > -0.625f)
		{
			//Down Swipe
			Debug.Log("Down swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.DOWN)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
		else if (angle < -0.875f || angle > 0.875f)
		{
			//Left Swipe
			Debug.Log("Left swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.LEFT)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
		else if (angle > -0.125f && angle < 0.125f)
		{
			//Right Swipe
			Debug.Log("Right swipe");
			if(nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.RIGHT)
            {
                DestroySwipObject(nextSwipeObject.gameObject);
				return;
            }
		}
		else if (angle > 0.125f && angle < 0.375f)
		{
			//Down Swipe
			Debug.Log("Top Right swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.TOP_RIGHT)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
		else if (angle > 0.625f && angle < 0.875f)
		{
			//Down Swipe
			Debug.Log("Top Left swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.TOP_LEFT)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
		else if (angle < -0.125f && angle > -0.375f)
		{
			//Down Swipe
			Debug.Log("Down Right swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.DOWN_RIGHT)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
		else if (angle < -0.625f && angle > -0.875f)
		{
			//Down Swipe
			Debug.Log("Down Left swipe");
			if (nextSwipeObject.IsVisible() && nextSwipeObject.m_swipeDirection == SwipeObject.eDirection.DOWN_LEFT)
			{
				DestroySwipObject(nextSwipeObject.gameObject);
				return;
			}
		}
	}

    private void DestroySwipObject(GameObject swipeObject)
    {
		float score = swipeObject.GetComponent<SwipeObject>().m_score;

        Destroy(swipeObject);

		if(OnSwipeObjectDestroyed != null)
        {
			Debug.Log("Score: " + score);
			OnSwipeObjectDestroyed(score);
		}
    }

	private SwipeObject GetNextSwipe()
    {
		SwipeObject[] swipeObjects = GameObject.FindObjectsOfType<SwipeObject>();
		SwipeObject nextSwipeObject = null;

		foreach (SwipeObject swipeObject in swipeObjects)
		{
			if (nextSwipeObject == null || nextSwipeObject.transform.position.y > swipeObject.transform.position.y)
			{
				nextSwipeObject = swipeObject;
			}
		}

		return nextSwipeObject;
	}
}
