﻿using System.Collections.Generic;
using UnityEngine;

public class AudioTrackData : MonoBehaviour
{
    public List<AudioClip> clips;
    public List<AudioTrack> tracks;
    public List<TextAsset> beatsheets;

    public void Awake()
    {
        tracks.Add(new AudioTrack()
        {
            AudioClip = clips[0],
            TrackName = "GameMusic",
            TrackLength = 204,
            TrackOutPoint = 90,
            TrackSpeed = 0.75f
        });

        tracks.Add(new AudioTrack()
        {
            AudioClip = clips[1],
            TrackName = "Between the Lines",
            TrackLength = 154,
            TrackOutPoint = 90,
            TrackSpeed = 0.5f
        });

        tracks.Add(new AudioTrack()
        {
            AudioClip = clips[2],
            TrackName = "Old to the New",
            TrackLength = 294,
            TrackOutPoint = 23,
            TrackSpeed = 0.5f,
            Beatsheet = beatsheets[0]
        });
    }
}
