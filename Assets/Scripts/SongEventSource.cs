using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongEndEventArgs : EventArgs
{
    public float SongLengthSeconds { get; set; }

    public SongEndEventArgs(float seconds)
    {
        SongLengthSeconds = seconds;
    }
}

public class SongEventSource : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioTrack currentSong;
    private readonly float FADE_LENGTH = 2.0f;

    public event EventHandler<SongEndEventArgs> OnSongEnded;
    private bool songEnded = false;

    void Awake()
    {
        currentSong = GameObject.Find("TrackInfo(Clone)").GetComponent<TrackInfo>().GetTrack();
        audioSource.clip = currentSong.AudioClip;
        audioSource.Play();
    }

    void Update()
    {
        if(audioSource.time >= currentSong.TrackOutPoint && !songEnded)
        {
            OnSongEnded(this, new SongEndEventArgs(currentSong.TrackOutPoint));
            songEnded = true;
        }

        float timePlayed = audioSource.time;
        float timePastFadeStart = timePlayed - (currentSong.TrackOutPoint - FADE_LENGTH);
        if (timePastFadeStart >= 0)
        {
            audioSource.volume = Mathf.Lerp(1, 0, timePastFadeStart / FADE_LENGTH);
        }
    }

    public int SwipeMode()
    {
        TrackInfo trackInfo = GameObject.Find("TrackInfo(Clone)").GetComponent<TrackInfo>();
        return trackInfo.swipeMode;
    }
}
