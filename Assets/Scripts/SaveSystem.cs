﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveSystem
{
    private static readonly string SCORENAME_DELIMITER = "_";
    private static readonly int SAVE_VERSION = 1;

    public static void SaveSongScore(string songTitle, float score, string grade)
    {
        string scoreName = GetScoreName(songTitle);

        PlayerPrefs.SetFloat(scoreName, score);
        string gradeName = GetGradeName(songTitle);
        PlayerPrefs.SetString(gradeName, grade);

        Debug.Log($"Saving {scoreName}, score {score}");

        //PlayerPrefs.Save(); //We shouldn't really need to do this, but because we're testing in editor we are
    }

    public static (float, string) LoadSongScore(string songTitle)
    {

        string scoreName = GetScoreName(songTitle);
        float score = PlayerPrefs.GetFloat(scoreName, 0);
        string gradeName = GetGradeName(songTitle);
        string grade = PlayerPrefs.GetString(gradeName, "F");

        Debug.Log($"Loading {scoreName}, score {score}");
        return (score, grade);
    }

    private static string GetScoreName(string songTitle) => $"SSE{SAVE_VERSION}{SCORENAME_DELIMITER}{songTitle.Replace(" ", "")}{SCORENAME_DELIMITER}Score";
    private static string GetGradeName(string songTitle) => $"SSE{SAVE_VERSION}{SCORENAME_DELIMITER}{songTitle.Replace(" ", "")}{SCORENAME_DELIMITER}Grade";
}
