﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreChangeEventArgs : EventArgs
{
    public int Score { get; set; }

    public ScoreChangeEventArgs(int score)
    {
        Score = score;
    }
}

public class ScoreSource : MonoBehaviour
{
    public int score = 0;
    public event EventHandler<ScoreChangeEventArgs> OnScoreChanged;

    private UserTouchToItem userTouchToItem;

    public void Start()
    {
        userTouchToItem = GameObject.Find("TouchInteractionHandler").GetComponent<UserTouchToItem>();
        userTouchToItem.OnSwipeObjectDestroyed += SwipeSuccessful;
    }

    public void OnDestroy()
    {
        userTouchToItem.OnSwipeObjectDestroyed -= SwipeSuccessful;
    }

    public void SwipeSuccessful(float distanceScore)
    {
        SetScore(score + (int)distanceScore);
    }

    private void SetScore(int newScore)
    {
        score = newScore;
        OnScoreChanged(this, new ScoreChangeEventArgs(score));
    }
}
