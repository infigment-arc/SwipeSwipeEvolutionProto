﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerIdentity : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnPlayerNameChanged))]
    public string playerName;
    [SyncVar(hook = nameof(OnPlayerScoreChanged))]
    public int score = 0;

    public Text playerNameText;
    public Text playerScoreText;

    private ScoreSource scoreSource;

    public void Start()
    {
        GameObject canvas = GameObject.Find("Canvas");
        canvas.GetComponent<PlayerHUDCollection>().JoinPlayerHUD(transform);
        scoreSource = canvas.GetComponent<ScoreSource>();
        scoreSource.OnScoreChanged += ScoreChange;
    }

    public void OnDestroy()
    {
        scoreSource.OnScoreChanged-= ScoreChange;
    }

    public override void OnStartLocalPlayer()
    {
        string name = "Player" + Random.Range(100, 999);
        SetName(name);
    }

    public void ScoreChange(object sender, ScoreChangeEventArgs eventArgs)
    {
        if(!isLocalPlayer)
        {
            return;
        }

        SetScore(eventArgs.Score);
    }

    [Command]
    private void SetScore(int newScore)
    {
        score = newScore;
    }

    [Command]
    private void SetName(string newName)
    {
        playerName = newName;
    }

    private void OnPlayerNameChanged(string oldValue, string newValue)
    {
        playerNameText.text = newValue;
    }

    private void OnPlayerScoreChanged(int oldValue, int newValue)
    {
        playerScoreText.text = newValue.ToString();
    }
}
